package com.example.ruan.rr1_crud_db.model;

/**
 * Created by ruan on 06/07/2016.
 */
public class VehicleModel {

    private int cod;
    private String brand;
    private String model;
    private String plate;
    private int year;

    public VehicleModel(){}

    public VehicleModel(int cod, String brand, String model, String plate, int year) {
        this.cod = cod;
        this.brand = brand;
        this.model = model;
        this.plate = plate;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }


}
