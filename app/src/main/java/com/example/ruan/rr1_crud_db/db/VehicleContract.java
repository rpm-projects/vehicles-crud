package com.example.ruan.rr1_crud_db.db;

import android.provider.BaseColumns;

/**
 * Created by ruan on 06/07/2016.
 */
public class VehicleContract {

    public VehicleContract(){}

    //Classe interna que define uma tabela e seu conteudo
    public static abstract class VehicleC implements BaseColumns {
        public final static String TABLE_NAME = "VEHICLE";
        public final static String BRAND = "brand";
        public final static String MODEL = "model";
        public final static String PLATE = "plate";
        public final static String YEAR = "year";
    }
}
