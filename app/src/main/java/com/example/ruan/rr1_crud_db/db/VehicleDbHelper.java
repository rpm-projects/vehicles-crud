package com.example.ruan.rr1_crud_db.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ruan on 06/07/2016.
 */
public class VehicleDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String NUMERIC_TYPE = " NUMERIC";
    private static final String INT_TYPE = " INT";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_TABLE_VEHICLE =
            "CREATE TABLE " + VehicleContract.VehicleC.TABLE_NAME + " (" +
                    VehicleContract.VehicleC._ID + " INTEGER PRIMARY KEY, " +
                    VehicleContract.VehicleC.BRAND + TEXT_TYPE + COMMA_SEP +
                    VehicleContract.VehicleC.MODEL + TEXT_TYPE + COMMA_SEP +
                    VehicleContract.VehicleC.PLATE + TEXT_TYPE + COMMA_SEP +
                    VehicleContract.VehicleC.YEAR + NUMERIC_TYPE +
                    " )";

    private static final String SQL_DELETE_TABLE_VEHICLE =
            "DROP TABLE IF EXISTS " + VehicleContract.VehicleC.TABLE_NAME;

    // Se você modificar o schema do banco, você deve incrementar a versão do software.
    public static final int DATABASE_VERSION = 26;
    public static final String DATABASE_NAME = "VEHICLE.db";

    public VehicleDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_TABLE_VEHICLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_TABLE_VEHICLE);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
