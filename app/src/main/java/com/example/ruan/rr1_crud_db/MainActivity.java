package com.example.ruan.rr1_crud_db;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.ruan.rr1_crud_db.db.VehicleDB;
import com.example.ruan.rr1_crud_db.model.VehicleModel;

import java.util.ArrayList;

public class MainActivity extends ListActivity {

    public static final String VEHICLE_COD = "com.example.ruan.rr1_crud_db.VEHICLE_COD";
    private ArrayList<VehicleModel> listVehicle = new ArrayList<VehicleModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

      /* Intent intent = getIntent();

        PersonDB personDB = new PersonDB(this);
        listPerson = personDB.findAllPerson();

        if (listPerson.size() > 0) {

            String query = intent.getStringExtra(SearchManager.QUERY);
            if(query != null && query.length() > 0) {
                if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

                    //use the query to search your data somehow
                    personDB = new PersonDB(this);
                    listPerson = personDB.findPersonByName(query);
                }
            }
            else{
                listPerson = personDB.findAllPerson();
            }

            setListAdapter(new PersonAdapter(this, listPerson));
        }*/

    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = getIntent();

        VehicleDB personDB = new VehicleDB(this);
        listVehicle = personDB.findAllVehicles();

        if (listVehicle.size() > 0) {

            String query = intent.getStringExtra(SearchManager.QUERY);
            if(query != null && query.length() > 0) {
                if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

                    //use the query to search your data somehow
                    personDB = new VehicleDB(this);
                    listVehicle = personDB.findVehicleByField(query);
                }
            }
            else{
                listVehicle = personDB.findAllVehicles();
            }

            setListAdapter(new VehicleAdapter(this, listVehicle));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =  (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int idItem = item.getItemId();


        if(R.id.addp == idItem){
            //chama tela de cadastro de pessoa
            startActivity(new Intent(this, VehicleCRUDActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    //Method that handles click on a list item
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        //get the item in specific position
        VehicleModel person = (VehicleModel) this.getListAdapter().getItem(position);
        int cod = person.getCod();
        String brand = person.getBrand();
        Toast.makeText(this, cod + " - " + brand, Toast.LENGTH_LONG).show();


        //if(item.equals("Tela Dois"))
        //   startActivity(new Intent(this, TelaDois.class));
        //Exibe um alerta
        //Toast.makeText(this, "Voce selecionou: " + item, Toast.LENGTH_LONG).show();

        //Get the person's ID that was clicked in the list and send it to
        //the responsible activity to find it in the DB to return its full details
        Bundle bundle = new Bundle();
        bundle.putString(VEHICLE_COD, String.valueOf(person.getCod()));
        startActivity(new Intent(this, FullDetailVehicleActivity.class).putExtras(bundle));
    }
}
