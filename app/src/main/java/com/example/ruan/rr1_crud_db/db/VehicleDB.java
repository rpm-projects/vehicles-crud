package com.example.ruan.rr1_crud_db.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ruan.rr1_crud_db.model.VehicleModel;

import java.util.ArrayList;

/**
 * Created by ruan on 06/07/2016.
 */
public class VehicleDB {

    VehicleDbHelper vdb = null;

    public VehicleDB(Context context){
        this.vdb = new VehicleDbHelper(context);
    }

    //Search all vehicles
    public ArrayList<VehicleModel> findAllVehicles() {

        ArrayList<VehicleModel> listVehicle = new ArrayList<VehicleModel>();
        VehicleModel vehicle = null;

        SQLiteDatabase db = this.vdb.getReadableDatabase();

        //Defina uma projeção que especifica quais colunas do banco de dados
        String[] projection = {
                VehicleContract.VehicleC._ID,
                VehicleContract.VehicleC.BRAND,
                VehicleContract.VehicleC.MODEL,
                VehicleContract.VehicleC.PLATE,
                VehicleContract.VehicleC.YEAR
        };

        //Como resultados serao ordenados no Cursor
        String sortOrder =
                VehicleContract.VehicleC._ID + " ASC";

        Cursor c = db.query(
                VehicleContract.VehicleC.TABLE_NAME,      // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.getCount() > 0){
            while(true) {
                c.moveToNext();
                vehicle = new VehicleModel();
                for (String coluna : projection) {
                    switch (coluna) {
                        case "_id":
                            vehicle.setCod(Integer.parseInt(c.getString(c.getColumnIndexOrThrow(coluna))));
                            break;
                        case "brand":
                            vehicle.setBrand(c.getString(c.getColumnIndexOrThrow(coluna)));
                            break;
                        case "model":
                            vehicle.setModel(c.getString(c.getColumnIndexOrThrow(coluna)));
                            break;
                        case "plate":
                            vehicle.setPlate(c.getString(c.getColumnIndexOrThrow(coluna)));
                            break;
                        case "year":
                            vehicle.setYear(Integer.parseInt(c.getString(c.getColumnIndexOrThrow(coluna))));
                            break;
                    }
                }

                listVehicle.add(vehicle);

                //If last then leave
                if (c.isLast()){
                    break;
                }
            }
        }

        return listVehicle;
    }

    //Search vehicle by field
    public ArrayList<VehicleModel> findVehicleByField(String dataToFilter) {

        ArrayList<VehicleModel> listVehicle = new ArrayList<VehicleModel>();
        VehicleModel vehicle = null;

        SQLiteDatabase db = this.vdb.getReadableDatabase();

        //Specify which columns will be used in select
        String[] projection = {
                VehicleContract.VehicleC._ID,
                VehicleContract.VehicleC.BRAND,
                VehicleContract.VehicleC.MODEL,
                VehicleContract.VehicleC.PLATE,
                VehicleContract.VehicleC.YEAR
        };

        //Columns to use in WHERE
        String selection = VehicleContract.VehicleC.BRAND + " like ?"
                            + " OR " +
                            VehicleContract.VehicleC.MODEL + " like ?"
                            + " OR " +
                            VehicleContract.VehicleC.PLATE + " like ?"
                            + " OR " +
                            VehicleContract.VehicleC.YEAR + " like ?";

        //Columna values for WHERE
        String[] selectionArgs = {
                dataToFilter + "%",  dataToFilter + "%",  dataToFilter + "%",  dataToFilter + "%"
        };
        //Order
        String sortOrder =
                VehicleContract.VehicleC._ID + " DESC";

        Cursor c = db.query(
                VehicleContract.VehicleC.TABLE_NAME,      // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        for(int i = 0; i < c.getCount(); i++){
            c.moveToFirst();
            vehicle = new VehicleModel();
            for(String coluna : projection){
                switch(coluna){
                    case "_id":
                        vehicle.setCod(Integer.parseInt(c.getString(c.getColumnIndexOrThrow(coluna))));
                        break;
                    case "brand":
                        vehicle.setBrand(c.getString(c.getColumnIndexOrThrow(coluna)));
                        break;
                    case "model":
                        vehicle.setModel(c.getString(c.getColumnIndexOrThrow(coluna)));
                        break;
                    case "plate":
                        vehicle.setPlate(c.getString(c.getColumnIndexOrThrow(coluna)));
                        break;
                    case "year":
                        vehicle.setYear(Integer.parseInt(c.getString(c.getColumnIndexOrThrow(coluna))));
                        break;
                }
            }

            listVehicle.add(vehicle);
        }

        return listVehicle;
    }

    //TODO - buscar vehicle pelo id
    public VehicleModel findVehicleById(int codVehicle) {

        VehicleModel vehicle = new VehicleModel();

        SQLiteDatabase db = this.vdb.getReadableDatabase();

        //Defina uma projeção que especifica quais colunas do banco de dados
        //você vai utilizar na query a seguir
        String[] projection = {
                VehicleContract.VehicleC._ID,
                VehicleContract.VehicleC.BRAND,
                VehicleContract.VehicleC.MODEL,
                VehicleContract.VehicleC.PLATE,
                VehicleContract.VehicleC.YEAR
        };

        //Colunas para filtrar no WHERE da query
        String selection = VehicleContract.VehicleC._ID + " = ?";
        //Valores para filtrar no WHERE da query, na mesma ordem das colunas
        String[] selectionArgs = {
                String.valueOf(codVehicle)
        };

        Cursor c = db.query(
                VehicleContract.VehicleC.TABLE_NAME,      // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if(c.getCount() > 0){
            c.moveToFirst();
            for(String coluna : projection){

                switch(coluna){
                    case "_id":
                        vehicle.setCod(Integer.parseInt(c.getString(c.getColumnIndexOrThrow(coluna))));
                        break;
                    case "brand":
                        vehicle.setBrand(c.getString(c.getColumnIndexOrThrow(coluna)));
                        break;
                    case "model":
                        vehicle.setModel(c.getString(c.getColumnIndexOrThrow(coluna)));
                        break;
                    case "plate":
                        vehicle.setPlate(c.getString(c.getColumnIndexOrThrow(coluna)));
                        break;
                    case "year":
                        vehicle.setYear(Integer.parseInt(c.getString(c.getColumnIndexOrThrow(coluna))));
                        break;
                }
            }
        }

        //return obj
        return vehicle;
    }
    public void insertVehicle(VehicleModel vehicle){
        // Instanciando um banco no modo de gravação
        SQLiteDatabase db = this.vdb.getWritableDatabase();

        //Criando um mapa de informações para serem gravadas no bnaco - conjunto de chave-valor
        ContentValues contentValues = new ContentValues();
        contentValues.put(VehicleContract.VehicleC.BRAND, vehicle.getBrand());
        contentValues.put(VehicleContract.VehicleC.MODEL, vehicle.getModel());
        contentValues.put(VehicleContract.VehicleC.PLATE, vehicle.getPlate());
        contentValues.put(VehicleContract.VehicleC.YEAR, vehicle.getYear());

        //Gravando informações no DB propriamente dito
        long newVehicleId = db.insert(
                VehicleContract.VehicleC.TABLE_NAME,
                null,   //The second argument provides the name of a column in which the framework can insert NULL in the event that the ContentValues is empty (if you instead set this to "null", then the framework will not insert a row when there are no values).
                contentValues);
    }

    //edit behicle info by id
    public void editVehicle(VehicleModel vehicle){
        // Instanciando um banco no modo de gravação
        SQLiteDatabase db = this.vdb.getWritableDatabase();

        //Criando um mapa de informações para serem gravadas no bnaco - conjunto de chave-valor
        ContentValues contentValues = new ContentValues();
        contentValues.put(VehicleContract.VehicleC.BRAND, vehicle.getBrand());
        contentValues.put(VehicleContract.VehicleC.MODEL, vehicle.getModel());
        contentValues.put(VehicleContract.VehicleC.PLATE, vehicle.getPlate());
        contentValues.put(VehicleContract.VehicleC.YEAR, vehicle.getYear());

        //Colunas para filtrar no WHERE da query
        String selection = VehicleContract.VehicleC._ID + " = ?";
        //Valores para filtrar no WHERE da query, na mesma ordem das colunas
        String[] selectionArgs = {
                String.valueOf(vehicle.getCod())
        };

        //Gravando informações no DB propriamente dito
        long numRowAffected = db.update(
                VehicleContract.VehicleC.TABLE_NAME,
                contentValues,
                selection,
                selectionArgs
        );
    }

    //delete behicle by id
    public void deleteVehicle(String vehicleCod) {

        // Instanciando um banco no modo de gravação
        SQLiteDatabase db = this.vdb.getWritableDatabase();

        //Colunas para filtrar no WHERE da query
        String selection = VehicleContract.VehicleC._ID + " = ?";
        //Valores para filtrar no WHERE da query, na mesma ordem das colunas
        String[] selectionArgs = {
                vehicleCod
        };

        //Gravando informações no DB propriamente dito
        long numRowAffected = db.delete(
                VehicleContract.VehicleC.TABLE_NAME,
                selection,
                selectionArgs
        );
    }
}
