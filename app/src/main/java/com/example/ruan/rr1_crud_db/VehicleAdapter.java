package com.example.ruan.rr1_crud_db;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ruan.rr1_crud_db.model.VehicleModel;

import java.util.List;

/**
 * Created by ruan on 07/07/2016.
 */
public class VehicleAdapter extends BaseAdapter {

    // protected static final String CATEGORIA = "livro";
    private Context context;
    private List<VehicleModel> lista;

    //constructor
    public VehicleAdapter(Context context, List<VehicleModel> lista) {
        this.context = context;
        this.lista = lista;
    }

    public int getCount() {
        return lista.size();
    }

    public Object getItem(int posicao) {
        VehicleModel vehicle = lista.get(posicao);
        //Log.i(CATEGORIA, "PostAdapter.getItem(" + posicao + ") > " + post);
        return vehicle;
    }

    public long getItemId(int posicao) {
        //Log.i(CATEGORIA,"PostAdapter.getItemId("+posicao+") > " + posicao);
        return posicao;
    }

    //config each linein the list with data
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.vehicle_list_row, parent, false);

        TextView cod = (TextView) row.findViewById(R.id.id_vehicle);
        TextView brand = (TextView) row.findViewById(R.id.brand_vehicle);
        TextView model = (TextView) row.findViewById(R.id.model_vehicle);
        TextView plate = (TextView) row.findViewById(R.id.plate_vehicle);
        TextView year = (TextView) row.findViewById(R.id.year_vehicle);

        cod.setText(String.valueOf(lista.get(position).getCod()));
        brand.setText(lista.get(position).getBrand());
        model.setText(lista.get(position).getModel());
        plate.setText(String.valueOf(lista.get(position).getPlate()));
        year.setText(String.valueOf(lista.get(position).getYear()));

        return (row);
    }
}