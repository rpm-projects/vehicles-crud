package com.example.ruan.rr1_crud_db;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ruan.rr1_crud_db.db.VehicleDB;
import com.example.ruan.rr1_crud_db.model.VehicleModel;

import java.util.ArrayList;

import static android.R.*;

/**
 * Created by ruan on 06/07/2016.
 */
public class VehicleCRUDActivity extends Activity {

    ArrayList<String> carModelList = new ArrayList<>();
    ArrayAdapter<String> adapter2;
    Spinner carrersSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_crud);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String codig = null;

        //Initial state brand spinner
        setSpinnerBrand();


        if(bundle != null) {
            codig = bundle.getString("CODIG");
        }

        //se a tela for chamada da tela fll detail entao vira com codig da pessoa
        // e entao ira setar os valores da pessoa nos campos
        if(codig != null && !codig.equals("")) {

            VehicleDB vehicleDB = new VehicleDB(this);
            VehicleModel vehicle = vehicleDB.findVehicleById(Integer.parseInt(codig));

            TextView textCod = (TextView) findViewById(R.id.cad_codig);

            Spinner brandSpinner = (Spinner) findViewById(R.id.cad_brand);
            Spinner modelSpinner = (Spinner) findViewById(R.id.cad_model);


            EditText editPlate = (EditText) findViewById(R.id.cad_plate);
            EditText editYear = (EditText) findViewById(R.id.cad_year);

            //seta valores na tela
            textCod.setText(String.valueOf(vehicle.getCod()));
            editPlate.setText(String.valueOf(vehicle.getPlate()));
            editYear.setText(String.valueOf(vehicle.getYear()));

            if(vehicle.getBrand().equals("FORD")){

                brandSpinner.setSelection(0);

                this.carModelList.add("KA");
                this.carModelList.add("FUSION");
                this.carModelList.add("FOCUS");
                this.carModelList.add("FIESTA");
            }
            else{

                brandSpinner.setSelection(1);

                this.carModelList.add("CORSA");
                this.carModelList.add("PRISMA");
                this.carModelList.add("ONIX");
                this.carModelList.add("CRUZE");
            }

            //[SPINNER CAR MODELS] -

            adapter2 = new ArrayAdapter<String>(this,
                    layout.simple_spinner_item, carModelList);
            modelSpinner.setAdapter(adapter2);

            //Check which model should be selected
            for(String model : carModelList){
                if(vehicle.getModel().equals(model)){
                    brandSpinner.setSelection(carModelList.indexOf(model));
                }
            }
        }
    }

    public void setSpinnerBrand(){
        //[SPINNER BRAND]
        final Spinner spinnerBrand = (Spinner) findViewById(R.id.cad_brand);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.carbrand, layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerBrand.setAdapter(adapter);

        final Spinner modelSpinner = (Spinner) findViewById(R.id.cad_model);


        spinnerBrand.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                        Object item = parent.getItemAtPosition(pos);
                        System.out.println(item.toString());     //prints the text in spinner item.

                        carModelList = new ArrayList<>();

                        if(item.toString().equals("FORD")){

                            spinnerBrand.setSelection(0);

                            carModelList.add("KA");
                            carModelList.add("FUSION");
                            carModelList.add("FOCUS");
                            carModelList.add("FIESTA");
                        }
                        else{

                            spinnerBrand.setSelection(1);

                            carModelList.add("CORSA");
                            carModelList.add("PRISMA");
                            carModelList.add("ONIX");
                            carModelList.add("CRUZE");
                        }

                        //[SPINNER CAR MODELS] -
                        handleSpinnerModel(modelSpinner);
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
    }


    public void handleSpinnerModel(Spinner modelSpinner){
        adapter2 = new ArrayAdapter<String>(this,
                layout.simple_spinner_item, carModelList);
        modelSpinner.setAdapter(adapter2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vehicle_crud, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_save){
            VehicleModel vehicle = new VehicleModel();

            //get info typed on fields

            int cod = Integer.parseInt(((TextView) findViewById(R.id.cad_codig)).getText().toString().equals("") ? "0" : ((TextView) findViewById(R.id.cad_codig)).getText().toString());
            String brand = ((Spinner) findViewById(R.id.cad_brand)).getSelectedItem().toString();
            String model = ((Spinner) findViewById(R.id.cad_model)).getSelectedItem().toString();
            String plate = (((EditText) findViewById(R.id.cad_plate)).getText()).toString();
            int year = Integer.parseInt((((EditText) findViewById(R.id.cad_year)).getText()).toString());

            //set values t the object that is supposed to be isnerted in db
            vehicle.setCod(cod);
            vehicle.setBrand(brand);
            vehicle.setModel(model);
            vehicle.setPlate(plate);
            vehicle.setYear(year);

            VehicleDB vehicleDB = new VehicleDB(this);

            //verifica se esta editando uma pessoa ou inserindo uma nova
            if(cod > 0){
                vehicleDB.editVehicle(vehicle);
            }
            else {
                //insert new vehicle in db
                vehicleDB.insertVehicle(vehicle);
            }

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        finish();

        return super.onOptionsItemSelected(item);
    }



}
