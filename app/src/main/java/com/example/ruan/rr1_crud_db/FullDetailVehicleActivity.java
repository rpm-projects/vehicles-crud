package com.example.ruan.rr1_crud_db;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.ruan.rr1_crud_db.db.VehicleDB;
import com.example.ruan.rr1_crud_db.model.VehicleModel;

public class FullDetailVehicleActivity  extends Activity {

    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_detail_vehicle);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();
        String personId = bundle.getString(MainActivity.VEHICLE_COD);

        //Busca registro no banco referente ao id da pessoa clicada na lista personId
        //Retorno e um obj de pessoa com sesus dados
        VehicleDB vehicleDB = new VehicleDB(this);
        VehicleModel vehicle = vehicleDB.findVehicleById(Integer.parseInt(personId));

        //Insert data on screen
        ((TextView) findViewById(R.id.detail_codig)).setText(String.valueOf(vehicle.getCod()));
        ((TextView) findViewById(R.id.detail_model)).setText(vehicle.getModel());
        ((TextView) findViewById(R.id.detail_plate)).setText(vehicle.getPlate());
        ((TextView) findViewById(R.id.detail_year)).setText(String.valueOf(vehicle.getYear()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_full_detail_vehicle, menu);

        return true;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //[EDIT]
        if (id == R.id.btn_menu_edit) {

            String cod = (((TextView) findViewById(R.id.detail_codig)).getText()).toString();
            String brand = (((TextView) findViewById(R.id.detail_brand)).getText()).toString();
            String model = (((TextView) findViewById(R.id.detail_model)).getText()).toString();
            String plate = (((TextView) findViewById(R.id.detail_plate)).getText()).toString();
            String year = (((TextView) findViewById(R.id.detail_year)).getText()).toString();

            Bundle bundle = new Bundle();
            bundle.putString("CODIG", cod);
            bundle.putString("BRAND", brand);
            bundle.putString("MODEL", model);
            bundle.putString("PLATE", plate);
            bundle.putString("YEAR", year);

            startActivity(new Intent(this, VehicleCRUDActivity.class).putExtras(bundle));

            return true;
        }
        else if(id == R.id.btn_menu_delete){

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            String vehicleCod = ((TextView) findViewById(R.id.detail_codig)).getText().toString();
                            VehicleDB vehicleDB = new VehicleDB(context);
                            vehicleDB.deleteVehicle(vehicleCod);
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you really want to delete this register?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


        }

        return super.onOptionsItemSelected(item);
    }
}
